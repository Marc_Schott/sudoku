package sudoku.utility;

import sudoku.model.Board;
import sudoku.model.EnforcedCellSaturator;
import sudoku.model.EnforcedNumberSaturator;
import sudoku.model.InvalidSudokuException;
import sudoku.model.SudokuBoardSolver;
import sudoku.model.SudokuSolver;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 * The interface between user and Turing Machine, based on text commands.
 */
public final class Shell {

    /**
     * The Turing Machine the Shell currently works with.
     */
    private static Board sudoku = null;

    /**
     * The instance solving the given Sudokus initialized with no solution
     * strategy.
     */
    private static SudokuSolver sudokuSolver = new SudokuBoardSolver();

    private static void error(String errorInformation) {
        System.out.println("Error! " + errorInformation);
    }

    /**
     * Utility class constructor preventing instantiation.
     */
    private Shell() {
        throw new UnsupportedOperationException(
                "Illegal call of utility class constructor.");
    }

    /**
     * Reads the file path or name, checks if it is quoted and removes the
     * quotation mark if necessary.
     *
     * @param scanner The scanner containing the file path.
     * @return The file path or name which can be taken by a file.
     */
    private static String getFilePath(Scanner scanner) {
        scanner.useDelimiter("\n");
        if (scanner.hasNext(Pattern.compile("\\s*\".+\"\\s*"))) {
            scanner.useDelimiter("\"");
            scanner.next();
            String pathToFile = scanner.next();
            scanner.useDelimiter("\"\\s*");
            return pathToFile;
        } else {
            scanner.useDelimiter("\\s+");
            return scanner.next();
        }
    }
    /**
     * Initializes a new Sudoku Board if the command and the file path were
     * given correctly and the file format was correct.
     *
     * @param scanner Contains the instructions of the user.
     * @throws IOException if an I/O error occurs.
     */
    private static void readSudokuFromFile(Scanner scanner)
            throws IOException {
        if (scanner.hasNext()) {
            String filePath = getFilePath(scanner);
            if (!scanner.hasNext()) {
                Path pathToSudoku = FileSystems.getDefault().getPath(filePath);
                if (Files.isRegularFile(pathToSudoku)
                        && Files.isReadable(pathToSudoku)) {
                    File sudokuFile = pathToSudoku.toFile();
                    try {
                        Shell.sudoku = SudokuFactory.loadFromFile(sudokuFile);
                    } catch (ParseException e) {
                        error("Invalid file format! " + e.getMessage());
                    } catch (InvalidSudokuException e) {
                        error("The given file specifies an invalid sudoku! "
                                + e.getMessage());
                    }
                } else {
                    error("The given file does not exist or is not "
                            + "accessible!");
                }
                scanner.useDelimiter("\\s+");
                return;
            }
        }
        error("Wrong parameter format! INPUT filepath expected! Use HELP for a "
                + "list of all available commands.");
        scanner.useDelimiter("\\s+");
    }

    /**
     * Prints all the possible solutions of the Sudoku that {@code sudokuSolver}
     * has found.
     *
     * @param scanner Contains the instructions of the user.
     * @throws IOException if an I/O error occurs.
     */
    private static void printAllSolutions(Scanner scanner) throws IOException {
        if (!scanner.hasNext()) {
            if (sudoku == null) {
                error("No Sudoku initialized yet!");
                return;
            }
            for (Board board : sudokuSolver.findAllSolutions(sudoku)) {
                System.out.println(board.toString());
            }
        } else {
            error("Wrong parameter format! ALL expected! Use HELP for a list "
                    + "of all available commands.");
        }
    }

    /**
     * Prints the first solution found by {@code sudokuSolver} or an error
     * message if no solution has been found.
     *
     * @param scanner Contains the instructions of the user.
     * @throws IOException if an I/O error occurs.
     */
    private static void printFirstSolution(Scanner scanner)
            throws IOException {
        if (!scanner.hasNext()) {
            if (sudoku == null) {
                error("No Sudoku initialized yet!");
                return;
            }
            Board firstSolution = sudokuSolver.findFirstSolution(sudoku);
            if (firstSolution == null) {
                System.out.println("Error! The Sudoku given was unsolvable!");
            } else {
                System.out.println(sudokuSolver.findFirstSolution(sudoku)
                        .prettyPrint());
            }
        } else {
            error("Wrong parameter format! FIRST expected! Use HELP for a "
                    + "list of all available commands.");
        }
    }

    /**
     * Prints the sudoku that {@code sudokuSolver} returns when using its
     * algorithms till a fixpoint is reached.
     *
     * @param scanner Contains the instructions of the user.
     * @throws IOException if an I/O error occurs.
     */
    private static void printSaturatedSudoku(Scanner scanner)
            throws IOException {
        if (!scanner.hasNext()) {
            if (sudoku == null) {
                error("No Sudoku initialized yet!");
                return;
            }
            Board saturatedSudoku = sudokuSolver.saturate(sudoku);
            if (saturatedSudoku != null) {
                System.out.println(saturatedSudoku.prettyPrint());
            } else {
                error("The Sudoku is unsolvable!");
            }
        } else {
            error("Wrong parameter format! SATURATE expected! Use HELP for a "
                    + "list of all available commands.");
        }
    }

    /**
     * Writes the Sudoku as a matrix to the console.
     *
     * @param scanner Contains the instructions of the user.
     * @throws IOException if an I/O error occurs.
     */
    private static void printSudoku(Scanner scanner) throws IOException {
        if (scanner.hasNext()) {
            error("Wrong parameter format! PRINT expected! Use HELP for a list "
                    + "of all available commands.");
        } else if (sudoku == null) {
            error("No Sudoku initialized yet!");
        } else {
            System.out.println(sudoku.prettyPrint());
        }
    }

    private static void help() {
        System.out.println("This program can read a sudoku from a file and "
                + "calculate solutions of it. The following instructions are"
                + "supported:");
        System.out.println("INPUT filepath | Reads a Sudoku from the given path"
                + "if possible");
        System.out.println("SATURATE | Uses the registered solvation "
                + "strategies in order to fill as many cells as possible and "
                + "prints the resulting board to the console.");
        System.out.println("FIRST | Searches in a deterministic way for a "
                + "solution of the Sudoku and prints it to the console.");
        System.out.println("ALL | Searches for all possible solutions of the "
                + "Sudoku and prints them as single rows to the console.");
        System.out.println("PRINT | Prints the current Sudoku to the console.");
        System.out.println("HELP | Shows this text.");
        System.out.println("QUIT | Stops the program.");
    }

    /**
     * Checks the instructions the user gives to the Shell and calls the right
     * methods to execute them.
     *
     * @param reader Reads the input from the user.
     * @throws IOException if an I/O error occurs.
     */
    private static void execute(BufferedReader reader) throws IOException {
        boolean continueInput = true;
        sudokuSolver.addSaturator(new EnforcedCellSaturator());
        sudokuSolver.addSaturator(new EnforcedNumberSaturator());
        while (continueInput) {
            System.out.print("sudoku> ");
            Scanner instructionScanner = new Scanner(reader.readLine());
            if (instructionScanner.hasNext()) {
                String command = instructionScanner.next();
                char instructor = command.toUpperCase().charAt(0);
                switch (instructor) {
                    case 'I':
                        readSudokuFromFile(instructionScanner);
                        break;
                    case 'P':
                        printSudoku(instructionScanner);
                        break;
                    case 'A':
                        printAllSolutions(instructionScanner);
                        break;
                    case 'F':
                        printFirstSolution(instructionScanner);
                        break;
                    case 'S':
                        printSaturatedSudoku(instructionScanner);
                        break;
                    case 'H':
                        help();
                        break;
                    case 'Q':
                        continueInput = false;
                        break;
                    default:
                        error("Invalid instruction! Use HELP for a list of "
                                + "the available instructions!");
                        break;
                }
            }
            instructionScanner.close();
        }
    }

    /**
     * Reads the instructions of the user and executes them.
     *
     * @param args Currently unused.
     * @throws IOException if a problem with a file or a Reader occurs.
     */
    public static void main(String[] args) throws IOException {
        BufferedReader inputReader = new BufferedReader(
                new InputStreamReader(System.in));
        Shell.execute(inputReader);
        inputReader.close();
    }
}